const client = require('../client')
const print = require('./print')

const exists = (index, params) => {
  client.indices.exists({index})
    .then(res => print(res))
}

const get = (index, params) => {
  client.indices.get({index})
    .then(res => print(res))
}

const del = (index, params) => {
  client.indices.delete({index})
    .then(res => print(res))
}

const getMapping = (index, params) => {
  client.indices.getMapping({index})
    .then(res => print(res))
}

const getSettings = (index, params) => {
  client.indices.getSettings({index})
    .then(res => print(res))
}

const subs = {exists, get, del, getMapping, getSettings}

module.exports = (sub, index, params) => {
  if(!subs[sub]) {
    console.log(`${sub} is not supported in indices module.` )
    process.exit(1)
  }
  subs[sub](index, params)
}
