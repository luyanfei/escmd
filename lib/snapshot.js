const client = require('../client')
const {print} = require('../util')

const status = params => {
  const [repository, snapshot] = params
  console.log('repository:' + repository + ',snapshot:' + snapshot)
  return client.snapshot.status({
    repository, snapshot
  })
}

const createRepository = params => {
  const [repository, location] = params
  return client.snapshot.createRepository({
    repository,
    body: {
      type: 'fs',
      settings: {
        location
      }
    }
  })
}

const create = params => {
  if(!params) {
    const usage = `snapshot create repository snapshot indices`
    console.log(usage)
    return Promise.resolve('wrong params')
  }
  const [repository, snapshot, indices] = params
  return client.snapshot.create({
    repository, snapshot,
    waitForCompletion: true,
    body: {
      indices
    }
  })
}

const restore = params => {
  const [repository, snapshot, indices] = params
  return client.snapshot.restore({
    repository, snapshot,
    waitForCompletion: true,
    body: {
      indices
    }
  })
}

const getRepository = repository => {
  return client.snapshot.getRepository({
    repository
  })
}

const deleteRepository = repository => {
  return client.snapshot.deleteRepository({
    repository
  })
}

const getSnapshot = params => {
  const [repository, snapshot] = params
  return client.snapshot.get({
    repository, snapshot
  })
}

const deleteSnapshot = params => {
  const [repository, snapshot] = params
  return client.snapshot.delete({
    repository, snapshot
  })
}

module.exports = {
  status, createRepository, create, getRepository,
  getSnapshot, deleteSnapshot, deleteRepository, restore
}
