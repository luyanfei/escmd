const inquirer = require('inquirer')
const fs = require('fs')
const util = require('util')
const configfile = require('../configfile')

const writeFile = util.promisify(fs.writeFile)

const questions = [
  {
    type: 'input',
    name: 'host',
    message: 'Please input elasticsearch host: '
  },
  {
    type: 'input',
    name: 'username',
    message: 'Please input username: '
  },
  {
    type: 'password',
    name: 'password',
    message: 'Pleast input password: '
  },
  {
    type: 'list',
    name: 'apiVersion',
    message: 'Please select api version: ',
    choices: ['6.0', '5.0'],
    default: 0
  }
]

module.exports = () => {
  inquirer.prompt(questions)
    .then(answers => {
      const {host, username, password, apiVersion} = answers
      let config = {
        host,
        httpAuth: `${username}:${password}`,
        apiVersion
      }
      return writeFile(configfile, JSON.stringify(config, null, ' '))
    })
    .catch(err => {
      console.log(err)
      process.exit(1)
    })
}
