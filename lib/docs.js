const client = require('../client')
const print = require('./print')

const update = (index, id, body) => {
  return client.update({
    index, type: index, id, body: JSON.parse(body)
  })
}

const subs = {update}

module.exports = (sub, index, id, body) => {
  if(!subs[sub]) {
    console.log(`${sub} is not supported in docs module.` )
    process.exit(1)
  }
  subs[sub](index, id, body).then(print).catch(print)
}
