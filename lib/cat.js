const client = require('../client')

const indices = () => {
  return client.cat.indices({format: 'json'})
    .then(results => results.filter(rs => !rs.index.startsWith('.')))
}

const repositories = () => {
  return client.cat.repositories({format: 'json'})
}

const snapshots = repository => client.cat.snapshots({
  repository, format: 'json'
})

const help = () => client.cat.help()


const count = params => client.cat.count()

module.exports = {indices, help, count, repositories, snapshots}
