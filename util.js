//const util = require('util')

const print = obj => {
  if(typeof obj === 'object') {
    console.log(JSON.stringify(obj, null, 2))
  } else {
    console.log(obj)
  }
}

const buildAction = subs => (sub, ...others) => {
  //console.log(util.inspect(others))
  const fn = subs[sub]
  if(!fn && typeof fn !== 'function') {
    console.log(`${sub} is not supported in this module.` )
    process.exit(1)
  }

  return fn(...others).then(print).catch(print)
}

module.exports = {buildAction, print}
