#!/usr/bin/env node

const program = require('commander')
const {buildAction} = require('./util')
const init = require('./lib/init')
const cat = require('./lib/cat')
const indices = require('./lib/indices')
const search = require('./lib/search')
const docs = require('./lib/docs')
const snapshot = require('./lib/snapshot')

program
  .version('0.0.1')
  .description('elasticsearch command line tools.')

program
  .command('init')
  .description('init elastic client configuration.')
  .action(() => init())

program
  .command('cat <sub> [params...]')
  .alias('c')
  .description('cat module.')
  .action(buildAction(cat))

program
  .command('indices <sub> <index> [params...]')
  .alias('i')
  .description('indices module.')
  .action(indices)

program
  .command('docs <sub> <index> <id> <body>')
  .alias('d')
  .option('--id', 'document id')
  .description('docs module.')
  .action(docs)

program
  .command('search <sub> <index> [params...]')
  .alias('s')
  .description('search module.')
  .action(search)

program
  .command('snapshot <sub> [params...]')
  .description('snapshot module')
  .action(buildAction(snapshot))

program.parse(process.argv)
