const elasticsearch = require('elasticsearch')
const fs = require('fs')
const configfile = require('./configfile')

const defaultConfig = {
  host: 'http://localhost:9200/',
  apiVersion: '6.0'
}

let configFromFile = JSON.parse(fs.readFileSync(configfile))

let config = configFromFile ? configFromFile : defaultConfig

module.exports = new elasticsearch.Client(config)
